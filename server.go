package main

import (
	"strings"
	"encoding/json"
	"labix.org/v2/mgo/bson"
	"net/http"
	"fmt"
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
	"labix.org/v2/mgo"
)

type resourceAttributes struct {
	Title string
}

type jsonConvertible interface {}
func main() {
	m := martini.Classic()
	m.Use(render.Renderer())
	m.Use(Mongo())
	m.Get("/", index)
	m.Get("/attributes/:resource", getAttributes)
	m.Run()
}

func index(r render.Render) {
	r.HTML(200, "hello", "endi")
}

func jsonString( obj jsonConvertible ) (s string) {
	jsonObj, err := json.Marshal( obj )

	if err != nil {
		s = ""
		} else {
			s = string( jsonObj )
		}

		return
	}

func getAttributes(params martini.Params, writer http.ResponseWriter, db *mgo.Database)(int, string){
	resource := strings.ToLower(params["resource"])
	fmt.Println(resource)
	writer.Header().Set("Content-Type", "application/json")

	var attrs []resourceAttributes
	db.C("test").Find(bson.M{"title": resource}).All(&attrs);

	if attrs !=nil {
		return http.StatusOK, jsonString(attrs)
	} else {
		return http.StatusNotFound, jsonString("No attributes found for resource: "+resource)
	}
}

func Mongo() martini.Handler {
	session, err := mgo.Dial("localhost:27017")
	if err != nil {
		panic(err)
	}

	return func(c martini.Context){
		reqSession := session.Clone()
		c.Map(reqSession.DB("test"))
		defer reqSession.Close()

		c.Next()
	}
}
